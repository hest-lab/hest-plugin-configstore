# Configstore plugin

[![pipeline status](https://gitlab.com/hest-lab/hest-plugin-configstore/badges/master/pipeline.svg)](https://gitlab.com/hest-lab/hest-plugin-configstore/-/commits/master)
[![coverage report](https://gitlab.com/hest-lab/hest-plugin-configstore/badges/master/coverage.svg)](https://gitlab.com/hest-lab/hest-plugin-configstore/-/commits/master)

A typesafe wrapper for [configstore](https://www.npmjs.com/package/configstore).

## Usage

```ts
// Create schema for the configuration
interface ISchema {
  foo: string
  bar: number
  oh: {
    dummy: string
  }
}

// Create instance
const hc = new HestConfigstore<ISchema>('your-project-name')
// Set a value
hc.set('foo', 'Hello World')
// Get a value
const val: string = hc.get('foo')
// Check if a key exists
const exists: boolean = hc.has('foo')
// Delete a specific key
hc.delete('bar')
// Clear all keys
hc.clear()
// Get the item count
const count: number = hc.count()
// Get the path to the config file
const path: string = hc.path()
```
